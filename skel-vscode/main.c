#include "defines.h"

void sillyDelay(int ms10)
{
    for (uint8_t i = 0; i < ms10; i++)
        _delay_ms(10);
}

int main(void)
{
    // OSCCAL = 0x6D;
#if (WDTON != 0)
    wdt_enable(WDTO_1S);
#else
    // wdt_disable();
#endif

    PORTD = 0xFF;
    DDRD = 0x00;

    PORTB = 0xFF;
    DDRB = 0x00;

    SET(DDR, LED_GREEN);
    SET(DDR, LED_RED);
    sei();
    for (;;)
    {
        wdr();
        CLR(PORT, LED_RED);
        sillyDelay(10);

        SET(PORT, LED_RED);
        sillyDelay(10);

        CLR(PORT, LED_RED);
        sillyDelay(10);

        SET(PORT, LED_RED);
        sillyDelay(10);

        CLR(PORT, LED_GREEN);
        sillyDelay(10);

        SET(PORT, LED_GREEN);
        sillyDelay(90);
    }
    return 0;
}

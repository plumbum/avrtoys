VS Code for AVR
===============

Install 
[CPP Tools](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools).

[Configure task](http://stackoverflow.com/questions/30269449/how-do-i-set-up-vscode-to-compile-c-code)
and setup shortcuts `File->Preferences->Keyboard Shortcuts`:

    { "key": "f8", "command": "workbench.action.tasks.build" },
    { "key": "ctrl+f8", "command": "workbench.action.tasks.runTask" }

Configuration and tasks already under `.vscode` dir.
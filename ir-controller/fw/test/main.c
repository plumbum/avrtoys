#include "defines.h"

void timerInit(void)
{
    TCCR0A = 0x00;
    TCCR0B = (0<<CS02) | (1<<CS01) | (0<<CS00);
    TIMSK = 1 << TOIE0; //Overflow Interrupt Enabled
}

volatile uint8_t _timerOverflow;
ISR(TIMER0_OVF_vect)
{
    _timerOverflow++;
}

uint16_t timerGet(void)
{
    uint16_t t;
    cli();
    t = _timerOverflow*256 + TCNT0;
    sei();
    return t;
}

int main(void)
{
    // OSCCAL = 0x6D;
#ifdef WDTON
    wdt_enable(WDTO_1S);
#else
    wdt_disable();
#endif


    PORTD = 0xFF;
    DDRD  = 0x00;

    PORTB = 0xFF;
    DDRB  = 0x00;

    SET(DDR, LED_INFO);
    SET(DDR, LED_2);

    timerInit();

    sei();
    for(;;)
    {
        wdr();
        CLR(PORT, LED_INFO);
        for(uint8_t i=0; i<10; i++) _delay_ms(10);
        SET(PORT, LED_INFO);
        for(uint8_t i=0; i<50; i++) _delay_ms(10);
    }
    return 0;
}


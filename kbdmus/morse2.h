#ifndef _MORSE_H_
#define _MORSE_H_

// A .-
// B -...
// C -.-.
// D -..
// E .
// F ..-.
// G --.
// H ....
// I ..
// J .---
// K -.-
// L ._..
// M --
// N -.
// O ---
// P .--.
// Q --.-
// R ._.
// S ...
// T -
// U ..-
// V ...-
// W .--
// X -..-
// Y -.--
// Z --..

#define M_DOT       0x01
#define M_DASH      0x02

#define M_A         ((M_DOT<<0) | (M_DASH<<2))
#define M_B         ((M_DASH<<0) | (M_DOT<<2) | (M_DOT<<4) | (M_DOT<<6))
#define M_C         ((M_DASH<<0) | (M_DOT<<2) | (M_DASH<<4) | (M_DOT<<6))
#define M_D         ((M_DASH<<0) | (M_DOT<<2) | (M_DOT<<4))
#define M_E         (M_DOT<<0)
#define M_F         ((M_DOT<<0) | (M_DOT<<2) | (M_DASH<<4) | (M_DOT<<6))
#define M_G         ((M_DASH<<0) | (M_DASH<<2) | (M_DOT<<4))
#define M_H         ((M_DOT<<0) | (M_DOT<<2) | (M_DOT<<4) | (M_DOT<<6))
#define M_I         ((M_DOT<<0) | (M_DOT<<2))
#define M_J         ((M_DOT<<0) | (M_DASH<<2) | (M_DASH<<4) | (M_DASH<<6))
#define M_K         ((M_DASH<<0) | (M_DOT<<2) | (M_DASH<<4))
#define M_L         ((M_DOT<<0) | (M_DASH<<2) | (M_DOT<<4) | (M_DOT<<6))
#define M_M         ((M_DASH<<0) | (M_DASH<<2))
#define M_N         ((M_DASH<<0) | (M_DOT<<2))
#define M_O         ((M_DASH<<0) | (M_DASH<<2) | (M_DASH<<4))
#define M_P         ((M_DOT<<0) | (M_DASH<<2) | (M_DASH<<4) | (M_DOT<<6))
#define M_Q         ((M_DASH<<0) | (M_DASH<<2) | (M_DOT<<4) | (M_DASH<<6))
#define M_R         ((M_DOT<<0) | (M_DASH<<2) | (M_DOT<<4))
#define M_S         ((M_DOT<<0) | (M_DOT<<2) | (M_DOT<<4))
#define M_T         (M_DASH<<0)
#define M_U         ((M_DOT<<0) | (M_DOT<<2) | (M_DASH<<4))
#define M_V         ((M_DOT<<0) | (M_DOT<<2) | (M_DOT<<4) | (M_DASH<<6))
#define M_W         ((M_DOT<<0) | (M_DASH<<2) | (M_DASH<<4))
#define M_X         ((M_DASH<<0) | (M_DOT<<2) | (M_DOT<<4) | (M_DASH<<6))
#define M_Y         ((M_DASH<<0) | (M_DOT<<2) | (M_DASH<<4) | (M_DASH<<6))
#define M_Z         ((M_DASH<<0) | (M_DASH<<2) | (M_DOT<<4) | (M_DOT<<6))

#define M_L0        M_E
#define M_L1        M_T
#define M_L2        M_I
#define M_L3        M_A
#define M_L4        M_N
#define M_L5        M_M
#define M_L6        M_S
#define M_L7        M_U
#define M_L8        M_R
#define M_L9        M_W

#endif /* _MORSE_H_ */


#ifndef _DEFINES_H_
#define _DEFINES_H_

/* Use watchdog */
#ifndef WDTON
#define WDTON 0
#endif

/* CPU frequency */
#ifndef F_CPU
#define F_CPU 8000000UL
#endif

#include <inttypes.h>
#include <ctype.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#define CYCLES_PER_US ((F_CPU+500000)/1000000) 	// cpu cycles per microsecond

#define HI(x) (uint8_t)((x)>>8)
#define LO(x) (uint8_t)((x) & 0xFF)

#define NOP __asm__ __volatile__ ("nop")

#if (WDTON != 0)
#   include <avr/wdt.h>
#   define wdr() wdt_reset()
#else
#   define wdr()
#endif


/* ******************* */
/* Bit access macroses */
/* ******************* */
#define GLUE(a, b)     a##b

/* single-bit macros, used for control bits */
#define SET_(what, p, m) GLUE(what, p) |= (1 << (m))
#define CLR_(what, p, m) GLUE(what, p) &= ~(1 << (m))
#define GET_(/* PIN, */ p, m) GLUE(PIN, p) & (1 << (m))
#define SET(what, x) SET_(what, x)
#define CLR(what, x) CLR_(what, x)
#define GET(/* PIN, */ x) GET_(x)

/* nibble macros, used for data path */
#define ASSIGN_(what, p, m, v) GLUE(what, p) = (GLUE(what, p) & \
						~((1 << (m)) | (1 << ((m) + 1)) | \
						  (1 << ((m) + 2)) | (1 << ((m) + 3)))) | \
					        ((v) << (m))
#define READ_(what, p, m) (GLUE(what, p) & ((1 << (m)) | (1 << ((m) + 1)) | \
					    (1 << ((m) + 2)) | (1 << ((m) + 3)))) >> (m)
#define ASSIGN(what, x, v) ASSIGN_(what, x, v)
#define READ(what, x) READ_(what, x)


/* **************************** */
/* Example pin definitions      */
#define LED_RED    B, 2
#define LED_GREEN  B, 3

/* HD44780 LCD port connections */
#define HD44780_RS D, 6
#define HD44780_RW D, 4
#define HD44780_E  D, 5
/* The data bits have to be not only in ascending order but also consecutive. */
#define HD44780_D4 B, 4


#endif /* _DEFINES_H_ */


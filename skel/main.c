#include "defines.h"

int main(void)
{
    // OSCCAL = 0x6D;
#if (WDTON != 0)
        wdt_enable(WDTO_1S);
#else
//        wdt_disable();
#endif


    PORTD = 0xFF;
    DDRD  = 0x00;

    PORTB = 0xFF;
    DDRB  = 0x00;

    SET(DDR, LED_GREEN);
    SET(DDR, LED_RED);
    sei();
    for(;;)
    {
        wdr();
        CLR(PORT, LED_RED);
        for(uint8_t i=0; i<10; i++) _delay_ms(10);
        SET(PORT, LED_RED);
        for(uint8_t i=0; i<10; i++) _delay_ms(10);
        CLR(PORT, LED_RED);
        for(uint8_t i=0; i<10; i++) _delay_ms(10);
        SET(PORT, LED_RED);
        for(uint8_t i=0; i<10; i++) _delay_ms(10);

        CLR(PORT, LED_GREEN);
        for(uint8_t i=0; i<10; i++) _delay_ms(10);
        SET(PORT, LED_GREEN);
        for(uint8_t i=0; i<90; i++) _delay_ms(10);
    }
    return 0;
}


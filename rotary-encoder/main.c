#include "defines.h"

/*
 * PWM control
 */

void pwm_init(void)
{
    TCNT0=0; // Reset counter
    OCR0A=0; // Init compare register A
    OCR0B=0; // Init compare register B
    TCCR0A = (0<<WGM01) | (1<<WGM00) // Phase correct PWM
        | (1<<COM0A1) | (0<<COM0A2); // Non inverse mode
    TCCR0B = (0<<CS02) | (1<<CS01) | (0<<CS00); // CLK/8
}

#define PWM_SET(level) do { OCR0A = (level); } while(0)

void pwm_set(uint8_t level)
{
    OCR0A=level;
}


/*
 * Rotary encoder control
 */

static uint8_t encoder_state;
static uint8_t encoder_value = 0;

static const PROGMEM int8_t encoder_actions[] = {
    /* B' A' B A */
    /* 0000 */ 0,
    /* 0001 */ +1,
    /* 0010 */ -1,
    /* 0011 */ 0,
    /* 0100 */ -1,
    /* 0101 */ 0,
    /* 0110 */ 0,
    /* 0111 */ +1,
    /* 1000 */ +1,
    /* 1001 */ 0,
    /* 1010 */ 0,
    /* 1011 */ -1,
    /* 1100 */ 0,
    /* 1101 */ -1,
    /* 1110 */ +1,
    /* 1111 */ 0,
};

ISR(PCINT_vect)
{
    uint8_t new_state = (1<<ENC_B) ? 2 : 0
        | (1<<ENC_A) ? 1 : 0
    uint8_t trans_state = new_state | (encoder_state<<2);
    int8_t inc = pgm_read_byte(encoder_actions + trans_state);
    if (inc < 0)
    {
        if (encoder_value > 0) encoder_value--;
        PWM_SET(encoder_value);
    }
    else if (inc > 0)
    {
        if (encoder_value < 255) encoder_value++;
        PWM_SET(encoder_value);
    }
    encoder_state = new_state;
}

void encoder_init(void)
{
    encoder_state = (1<<ENC_B) ? 2 : 0
        | (1<<ENC_A) ? 1 : 0;

    PCMSK = (1<<ENC_B) | (1<<ENC_A);
    GIMSK |= (1<<PCIE); // Enable pin changed interrupt
}



int main(void)
{
    // OSCCAL = 0x6D;
#if (WDTON != 0)
    wdt_enable(WDTO_1S);
#else
    // wdt_disable();
#endif

    PORTB = (1<<ENC_A) | (1<<ENC_B);
    DDRB = (1<<PWM_OUT);

    sei();
    for (;;)
    {
        wdr();
        CLR(PORT, LED_RED);
        sillyDelay(10);

        SET(PORT, LED_RED);
        sillyDelay(10);

        CLR(PORT, LED_RED);
        sillyDelay(10);

        SET(PORT, LED_RED);
        sillyDelay(10);

        CLR(PORT, LED_GREEN);
        sillyDelay(10);

        SET(PORT, LED_GREEN);
        sillyDelay(90);
    }
    return 0;
}

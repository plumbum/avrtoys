#ifndef _DEFINES_H_
#define _DEFINES_H_

/* Use watchdog */
#ifndef WDTON
#define WDTON 0
#endif

/* CPU frequency */
#ifndef F_CPU
#define F_CPU 8000000UL
#endif

#include <inttypes.h>
#include <ctype.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#define CYCLES_PER_US ((F_CPU + 500000) / 1000000) // cpu cycles per microsecond

#define HI(x) (uint8_t)((x) >> 8)
#define LO(x) (uint8_t)((x)&0xFF)

#define NOP __asm__ __volatile__("nop")

#if (WDTON != 0)
#include <avr/wdt.h>
#define wdr() wdt_reset()
#else
#define wdr()
#endif

/* ******************* */
/* Bit access macroses */
/* ******************* */
#define GLUE(a, b) a##b

/* single-bit macros, used for control bits */
#define SET_(what, p, m) GLUE(what, p) |= (1 << (m))
#define CLR_(what, p, m) GLUE(what, p) &= ~(1 << (m))
#define GET_(/* PIN, */ p, m) GLUE(PIN, p) & (1 << (m))
#define SET(what, x) SET_(what, x)
#define CLR(what, x) CLR_(what, x)
#define GET(/* PIN, */ x) GET_(x)

/* nibble macros, used for data path */
#define ASSIGN_(what, p, m, v) GLUE(what, p) = (GLUE(what, p) &                           \
                        ~((1 << (m)) | (1 << ((m) + 1)) |         \
                          (1 << ((m) + 2)) | (1 << ((m) + 3)))) | \
                           ((v) << (m))
#define READ_(what, p, m) (GLUE(what, p) & ((1 << (m)) | (1 << ((m) + 1)) |          \
                        (1 << ((m) + 2)) | (1 << ((m) + 3)))) >> \
                  (m)
#define ASSIGN(what, x, v) ASSIGN_(what, x, v)
#define READ(what, x) READ_(what, x)

/* **************************** */
/* Pin definitions      */

/*
        +-----------------+
        | 1 nRST    VCC 8 |
Enc A   | 2 PB3     PB2 7 | ADC1 Photo sensor
Enc B   | 3 PB4     PB1 6 | INT0 Enc push
        | 4 GND     PB0 5 | OC0A PWM
        +-----------------+

*/

#define PWM_OUT     0
#define ENC_PUSH    1
#define ENC_A       3
#define ENC_B       4

#define PHOTO_PIN   2
#define PHOTO_ADC   1

#endif /* _DEFINES_H_ */

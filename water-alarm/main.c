#include "defines.h"

#include <avr/sleep.h>

#define WATER_ALARM     (128)

/*
 * 4.5v ~ 238
 * 3.5v ~ 184
 * 3.3v ~ 174
 * 19mv per 1
 */
#define BATTERY_ALARM   (174)
#define TONE_US         (400)

// #define DEBUG

uint8_t read_water(void)
{
    uint8_t adc_result;

    // Set the ADC input to PB3/ADC3, no left adjust result
    ADMUX = (1<<MUX1) | (1<<MUX0) | (1<<ADLAR);

    // Set the prescaler to clock/128 & enable ADC
    // At 9.6 MHz this is 75 kHz.
    // See ATtiny13 datasheet, Table 14.4.
    ADCSRA = (1<<ADPS1) | (1<<ADPS0) | (1<<ADEN);

    _delay_us(10);

    // Start the next conversion
    ADCSRA |= (1<<ADSC);

    while (ADCSRA & (1<<ADSC)) wdr(); // Replace with sleep

    adc_result = ADCH;

    ADCSRA = 0;
    return adc_result;
}

uint8_t read_voltage(void)
{
    uint8_t adc_result;

    // Set the ADC input to PB4/ADC2, no left adjust result
    ADMUX = (1<<MUX1) | (0<<MUX0) | (1<<ADLAR) | (1<<REFS0);

    // Set the prescaler to clock/128 & enable ADC
    // At 9.6 MHz this is 75 kHz.
    // See ATtiny13 datasheet, Table 14.4.
    ADCSRA = (1<<ADPS1) | (1<<ADPS0) | (1<<ADEN);

    _delay_us(10);

    // Start the next conversion
    ADCSRA |= (1<<ADSC);

    while (ADCSRA & (1<<ADSC)) wdr(); // Replace with sleep

    adc_result = ADCH;

    ADCSRA = 0;
    return adc_result;

}

static void gpio_setup(void)
{
    PORTB = (0<<OUT_OC0A) | (0<<OUT_OC0B) | (1<<OUT_POWER) | (0<<IN_ADC3) | (0<<IN_ADC2);
    DDRB  = (1<<OUT_OC0A) | (1<<OUT_OC0B) | (1<<OUT_POWER) | (0<<IN_ADC3) | (0<<IN_ADC2);
}

static void hw_setup(void)
{
    ACSR  = (1<<ACD);
    gpio_setup();
    // sei();
}

static void power_down(void)
{
    cli();
    DDRB  = 0;
    PORTB = 0;
    ACSR  = (1<<ACD);
    ADCSRA = 0;
    PRR = (1<<PRTIM0) | (1<<PRADC);
    // BODCR = (1<<BODS) | (1<<BODSE);
    // BODCR = (1<<BODS);

    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    for (;;) {
        sleep_enable();
        sleep_cpu();
    }

    // Never will execute below
    gpio_setup();
    for (;;) {
        PORTB |= (1<<OUT_OC0A);
        PORTB &= ~(1<<OUT_OC0A);
    }
}

#ifdef DEBUG
void debug_put(uint8_t val)
{
    uint8_t i=7;
    for(;;)
    {
        if(val & 0x80)
        {
            PORTB |= (1<<OUT_OC0A);
            PORTB |= (1<<OUT_OC0A);
            PORTB |= (1<<OUT_OC0A);
            PORTB |= (1<<OUT_OC0A);
            PORTB &= ~(1<<OUT_OC0A);
        }
        else
        {
            PORTB |= (1<<OUT_OC0A);
            PORTB &= ~(1<<OUT_OC0A);
            PORTB &= ~(1<<OUT_OC0A);
            PORTB &= ~(1<<OUT_OC0A);
            PORTB &= ~(1<<OUT_OC0A);
        }
        if (i == 0) return;
        val <<= 1;
        i--;
    }
}
#endif

#define beep_ms(ms) beep_raw(1000UL*(ms)/TONE_US)

void beep_raw(uint16_t delay)
{
    DDRB |= (1<<OUT_OC0A) | (1<<OUT_OC0B);
    do
    {
        wdr();
        PORTB |= (1<<OUT_OC0A);
        PORTB &= ~(1<<OUT_OC0B);
        _delay_us(TONE_US/2);
        PORTB |= (1<<OUT_OC0B);
        PORTB &= ~(1<<OUT_OC0A);
        _delay_us(TONE_US/2);
    } while(delay--);
    PORTB &= ~(1<<OUT_OC0B);
    DDRB &= ~((1<<OUT_OC0A) | (1<<OUT_OC0B));
}

int main(void)
{
    // OSCCAL = 0x6D;
#if (WDTON != 0)
    wdt_enable(WDTO_2S);
#else
    // wdt_disable();
#endif

    hw_setup();
    _delay_us(100);

    uint8_t val_water = read_water();
    uint8_t val_voltage = read_voltage();

#ifdef DEBUG
    debug_put(val_water);
    for(int i=0; i<9; i++) asm("nop");
    debug_put(val_voltage);
#endif

    if (val_water < WATER_ALARM)
    {
        beep_ms(200);
        _delay_ms(50);
        beep_ms(100);
        _delay_ms(50);
        beep_ms(100);
        _delay_ms(50);
        beep_ms(100);
    } else
    if (val_voltage < BATTERY_ALARM)
    {
        beep_ms(25);
        _delay_ms(50);
        beep_ms(25);
    }


    power_down();

    for (;;) { }
    return 0;
}
